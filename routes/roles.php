<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 3:05 AM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {

        /***************************Secured Endpoint***********************/

        $api->get('roles',  'App\Http\Controllers\RoleController@show');

        $api->get('permissions',  'App\Http\Controllers\PermissionController@show');

        $api->get('roles/{id}',  'App\Http\Controllers\RoleController@get');

        $api->get('permissions/{id}',  'App\Http\Controllers\PermissionController@get');

        $api->post('roles',  'App\Http\Controllers\RoleController@create');

        $api->post('roles/{role_id}/permissions',  'App\Http\Controllers\RoleController@attachPermission');

        $api->post('permissions',  'App\Http\Controllers\PermissionController@create');

        $api->put('roles/{id}',  'App\Http\Controllers\RoleController@update');

        $api->put('permissions/{id}',  'App\Http\Controllers\PermissionController@update');

        $api->delete('roles/{id}',  'App\Http\Controllers\RoleController@delete');

        $api->delete('roles/{role_id}/permissions/{permission_id}',  'App\Http\Controllers\RoleController@detachPermission');

        $api->delete('permissions/{id}',  'App\Http\Controllers\PermissionController@delete');
    });
});