<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */

        /*************** countries **************/
        $api->post('countries', 'App\Http\Controllers\LocationController@storeCountry');

        $api->put('countries/{country_id}', 'App\Http\Controllers\LocationController@updateCountry');

        /*************** states **************/
        $api->post('states', 'App\Http\Controllers\LocationController@storeState');

        $api->put('states/{id}', 'App\Http\Controllers\LocationController@updateState');

        /*************** cities **************/
        $api->post('cities', 'App\Http\Controllers\LocationController@storeCity');

        $api->put('cities/{id}', 'App\Http\Controllers\LocationController@updateCity');

    });

    /**
     * Unsecured endpoint for country
     */
     $api->get('countries', 'App\Http\Controllers\LocationController@showCountries');
     $api->get('countries/{country_id}', 'App\Http\Controllers\LocationController@getCountries');
     /**
     * Unsecured endpoint for states
     */

     $api->get('states', 'App\Http\Controllers\LocationController@showStates');
     $api->get('states/{state_id}', 'App\Http\Controllers\LocationController@getStates');
     /**
     * Unsecured endpoint for cities
     */

     $api->get('cities', 'App\Http\Controllers\LocationController@showCities');
     $api->get('cities/{city_id}', 'App\Http\Controllers\LocationController@getCities');
});