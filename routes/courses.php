<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */
        $api->post('courses',  'App\Http\Controllers\CourseController@store');
        //update programmes
        $api->put('courses/{id}',  'App\Http\Controllers\CourseController@update');
    });

    /**
     * Unsecured endpoint Routes
     */
    //return all departments
    $api->get('courses', 'App\Http\Controllers\CourseController@show');
    //return a department by id
    $api->get('courses/{id}', 'App\Http\Controllers\CourseController@get');
});