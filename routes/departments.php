<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */
        $api->post('departments',  'App\Http\Controllers\DepartmentController@store');
        //update programmes
        $api->put('departments/{id}',  'App\Http\Controllers\DepartmentController@update');
        //add course to department
        $api->post('departments/{id}/courses',  'App\Http\Controllers\DepartmentController@addCourses');
        //remove department from course
        $api->delete('departments/{id}/courses/{course_id}',  'App\Http\Controllers\DepartmentController@removeCourse');
    });

    /**
     * Unsecured endpoint Routes
     */
    //return all departments
    $api->get('departments', 'App\Http\Controllers\DepartmentController@show');
    //return a department by id
    $api->get('departments/{id}', 'App\Http\Controllers\DepartmentController@get');
});