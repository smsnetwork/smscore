<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */
        $api->post('sessions',  'App\Http\Controllers\SessionController@store');
        //update programmes
        $api->put('sessions/{id}',  'App\Http\Controllers\SessionController@update');

        //Semester Secured Endpoint
        $api->post('semesters',  'App\Http\Controllers\SemesterController@store');
        $api->put('semesters/{id}',  'App\Http\Controllers\SemesterController@update');
    });

    /**
     * Unsecured endpoint Routes
     */
    //return all departments
    $api->get('sessions', 'App\Http\Controllers\SessionController@show');
    //return a department by id
    $api->get('sessions/{id}', 'App\Http\Controllers\SessionController@get');

    // Semester Endpoint
    /**
     * Unsecured endpoint Routes
     */
    //return all departments
    $api->get('semesters', 'App\Http\Controllers\SemesterController@show');
    //return a department by id
    $api->get('semesters/{id}', 'App\Http\Controllers\SemesterController@get');
});