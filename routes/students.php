<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */

        $api->get('students/{student_id}',  'App\Http\Controllers\StudentController@get');

        $api->post('students/{student_id}/courses',  'App\Http\Controllers\StudentController@createCourse');

        $api->post('students/{student_id}/departments',  'App\Http\Controllers\StudentController@createDepartment');

        $api->put('students/{student_id}/departments',  'App\Http\Controllers\StudentController@updateDepartment');

        $api->post('students/{student_id}/levels',  'App\Http\Controllers\StudentController@attachLevel');

        $api->post('students/{student_id}/ssce',  'App\Http\Controllers\StudentController@createSsce');

        $api->post('students/{student_id}/jamb',  'App\Http\Controllers\StudentController@createJamb');

        $api->post('students/{student_id}/associates',  'App\Http\Controllers\StudentController@createRelations');
    });

    $api->post('students/login',  'App\Http\Controllers\StudentController@login');
});