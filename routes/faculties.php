<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */
        $api->post('faculties',  'App\Http\Controllers\FacultyController@store');
        //update programmes
        $api->put('faculties/{id}',  'App\Http\Controllers\FacultyController@update');
    });

    /**
     * Unsecured endpoint Routes
     */
    //return all users
    $api->get('faculties', 'App\Http\Controllers\FacultyController@show');
    //return all users
    $api->get('faculties/{id}', 'App\Http\Controllers\FacultyController@get');
});