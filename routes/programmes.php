<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */
        $api->post('programmes',  'App\Http\Controllers\ProgrammeController@store');
        //update programmes
        $api->put('programmes/{id}',  'App\Http\Controllers\ProgrammeController@update');
    });

    /**
     * Unsecured endpoint Routes
     */
    //return all users
    $api->get('programmes', 'App\Http\Controllers\ProgrammeController@show');
    //return all users
    $api->get('programmes/{id}', 'App\Http\Controllers\ProgrammeController@get');
});