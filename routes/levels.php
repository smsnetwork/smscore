<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */
        $api->get('levels',  'App\Http\Controllers\levelController@show');
        //update programmes
        $api->get('levels/{id}',  'App\Http\Controllers\levelController@get');

        //Semester Secured Endpoint
        $api->post('levels',  'App\Http\Controllers\levelController@store');
        $api->put('levels/{id}',  'App\Http\Controllers\levelController@update');
    });
});