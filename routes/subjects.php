<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:08 PM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Secure Endpoint
         */

        $api->post('subjects',  'App\Http\Controllers\SubjectController@store');
    });

    $api->get('subjects',  'App\Http\Controllers\SubjectController@show');
    $api->get('subjects/{subject_id}',  'App\Http\Controllers\SubjectController@get');
});