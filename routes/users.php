<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 3:05 AM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {
        /**
         * Users Routes
         */
        //return all users
        $api->get('users', 'App\Http\Controllers\UserController@show');

        //return all users
        $api->get('users/{id}', 'App\Http\Controllers\UserController@get');

        $api->post('users/{user_id}/addresses',  'App\Http\Controllers\UserController@addAddress');
    });
    //create users
    $api->post('users', 'App\Http\Controllers\UserController@store');
});