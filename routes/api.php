<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

require base_path('routes/users.php');
require base_path('routes/programmes.php');
require base_path('routes/faculties.php');
require base_path('routes/departments.php');
require base_path('routes/courses.php');
require base_path('routes/sessions.php');
require base_path('routes/students.php');
require base_path('routes/levels.php');
require base_path('routes/addresses.php');
require base_path('routes/locations.php');
require base_path('routes/subjects.php');
require base_path('routes/roles.php');