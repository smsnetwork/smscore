<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 3:05 AM
 */

$api = app('Dingo\Api\Routing\Router');

$api->version("v1", function ($api) {
    $api->group(['middleware' => ['auth:api', 'bindings']], function ($api) {

        /***************************Secured Endpoint***********************/
        $api->post('addresses',  'App\Http\Controllers\AddressController@create');

        $api->put('addresses/{id}',  'App\Http\Controllers\AddressController@update');
    });

    /***************************Unsecured Endpoint***********************/

    $api->get('addresses',  'App\Http\Controllers\AddressController@show');

    $api->get('addresses/{id}',  'App\Http\Controllers\AddressController@get');
});