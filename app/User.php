<?php

namespace App;

use App\Models\Address;
use App\Models\Course;
use App\Models\Department;
use App\Models\Jamb;
use App\Models\Level;
use App\Models\Ssce;
use App\Models\Subject;
use App\Models\UserAssociate;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'student_id', 'matric_number', 'first_name', 'last_name', 'middle_name', 'gender', 'birthday', 'email',
        'password', 'mobile', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return mixed
     */
    public function departments()
    {
        return $this->belongsToMany(Department::class, 'users_departments', 'user_id', 'department_id')
            ->withPivot(['user_id', 'department_id', 'session']);
    }

    /**
     * @return mixed
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'users_courses', 'user_id', 'course_id')
            ->withPivot(['user_id', 'department_id', 'session', 'semester']);
    }

    /**
     * @return mixed
     */
    public function levels()
    {
        return $this->belongsToMany(Level::class, 'users_levels', 'user_id', 'level_id')
            ->withPivot(['user_id', 'level_id', 'session']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ssce()
    {
        return $this->belongsToMany(Ssce::class, 'ssce_results', 'user_id', 'ssce_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jambs()
    {
        return $this->belongsToMany(Jamb::class, 'jambs_results', 'user_id', 'jamb_id');
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, "ssce_results", 'user_id', 'subject_id');
    }

    public function userAssociate()
    {
        return $this->hasMany(UserAssociate::class, 'user_id', 'id');
    }
}
