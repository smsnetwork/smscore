<?php

namespace App\Http\Controllers;

use App\Http\Requests\Ssce\StoreSsceRequest;
use App\Service\Repository\SsceService;
use App\Service\Transformer\SsceTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

use App\Http\Requests;

class SsceController extends Controller
{
    protected $ssceService;

    public function __construct(SsceService $ssceService)
    {
        $this->ssceService = $ssceService;
    }

    public function show()
    {

    }

    public function get()
    {

    }

    public function create(StoreSsceRequest $request)
    {
        $ssce = $this->ssceService->create($request->all());
        if(!$ssce) {
            throw new StoreResourceFailedException("Could not create", [
                "message" => "Could not create Ssce"
            ]);
        }
        return $this->response->item($ssce, new SsceTransformer);
    }
}
