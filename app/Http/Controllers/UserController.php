<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreUserPost;
use App\Service\Repository\UserService;
use App\Service\Transformer\UserTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function show()
    {
        return $this->response->collection($this->userService->all(), new UserTransformer);
    }

    public function get($id)
    {
        return $this->response->item($this->userService->get($id)->first(), new UserTransformer);
    }

    public function store(StoreUserPost $request)
    {
        return $this->response->item($this->userService->create($request->all()), new UserTransformer);
    }
}