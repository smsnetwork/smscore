<?php

namespace App\Http\Controllers;

use App\Service\Repository\CountryService;
use App\Service\Transformer\CountryTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;

class CountryController extends Controller
{
    protected $countryService;

    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->levelService->all(), new CountryTransformer);
    }
}
