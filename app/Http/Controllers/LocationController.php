<?php

namespace App\Http\Controllers;

use App\Http\Requests\Location\StoreCityLocation;
use App\Http\Requests\Location\StoreCountryLocation;
use App\Http\Requests\Location\StoreStateLocation;
use App\Http\Requests\Location\UpdateCityLocation;
use App\Http\Requests\Location\UpdateCountryLocation;
use App\Http\Requests\Location\UpdateStateLocation;
use App\Service\Repository\CityService;
use App\Service\Repository\CountryService;
use App\Service\Repository\StateService;
use App\Service\Transformer\CityTransformer;
use App\Service\Transformer\CountryTransformer;
use App\Service\Transformer\StateTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Fractal\Manager;

class LocationController extends Controller
{
    protected $countryService;
    protected $stateService;
    protected $cityService;
    protected $manager;

    public function __construct(CountryService $countryService, StateService $stateService, CityService $cityService, Manager $manager)
    {
        $this->countryService = $countryService;
        $this->stateService = $stateService;
        $this->cityService = $cityService;
        $this->manager = $manager;
    }

    /******************************************************************
     ****************Country Controller********************************
     ******************************************************************/

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function showCountries(Request $request)
    {
        if ($request->has("include")) {
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->countryService->all(), new CountryTransformer);
    }

    /**
     * @param StoreCountryLocation $request
     * @return \Dingo\Api\Http\Response
     */
    public function storeCountry(StoreCountryLocation $request)
    {
        $response = $this->countryService->create($request->all());
        if (!$response) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "Unable to create Country"
            ]);
        }
        return $this->response->item($response, new CountryTransformer);
    }

    /**
     * @param UpdateCountryLocation $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function updateCountry(UpdateCountryLocation $request, $id)
    {
        $response = $this->countryService->update($request->all(), $id);
        if (!$response) {
            throw new StoreResourceFailedException('Can not update.', [
                "message" => "Unable to update Country"
            ]);
        }
        return $this->response->item($response, new CountryTransformer);
    }

    /******************************************************************
     ****************State Controller********************************
     ******************************************************************/

    public function showStates(Request $request)
    {
        if ($request->has("include")) {
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->stateService->all(), new StateTransformer);
    }

    public function storeState(StoreStateLocation $request)
    {
        $response = $this->stateService->create($request->all());
        if (!$response) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "State cannot be created, "
            ]);
        }
        return $this->response->item($response, new StateTransformer);
    }

    public function updateState(UpdateStateLocation $request, $id)
    {
        $state = $this->stateService->update($request->all(), $id);
        if (!$state) {
            throw new StoreResourceFailedException('Can not update.', [
                "message" => "Unable to update state"
            ]);
        }
        return $this->response->item($state, new StateTransformer);
    }

    /******************************************************************
     ****************City Controller********************************
     ******************************************************************/

    public function showCities(Request $request)
    {
        if ($request->has("include")) {
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->cityService->all(), new CityTransformer);
    }

    public function storeCity(StoreCityLocation $request)
    {
        $response = $this->cityService->create($request->all());
        if (!$response) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "City cannot be created, "
            ]);
        }
        return $this->response->item($response, new CityTransformer);
    }

    public function updateCity(UpdateCityLocation $request, $id)
    {
        $city = $this->cityService->update($request->all(), $id);
        if (!$city) {
            throw new StoreResourceFailedException('Can not update.', [
                "message" => "Unable to update city"
            ]);
        }
        return $this->response->item($city, new CityTransformer);
    }
}
