<?php

namespace App\Http\Controllers;

use App\Service\Repository\LevelService;
use App\Service\Transformer\LevelTransformer;
use App\Service\Validator\LevelValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Fractal\Manager;

class LevelController extends Controller
{
    protected $levelService;
    protected $levelValidator;
    protected $manager;

    public function __construct(LevelService $levelService, LevelValidator $levelValidator, Manager $manager)
    {
        $this->levelService = $levelService;
        $this->levelValidator = $levelValidator;
        $this->manager = $manager;
    }

    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->levelService->all(), new LevelTransformer);
    }

    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->item($this->levelService->get($id), new LevelTransformer);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request)
    {
        $this->levelValidator->create();
        $level = $this->levelService->create($request->all());
        return $this->response->item($level, new LevelTransformer);
    }

    public function update(Request $request, $id)
    {
        $this->levelValidator->update();
        $level = $this->levelService->get($id);
        $level->update(array_only($request->all(), [
            "name", "code"
        ]));
        return $this->response->item($level, new LevelTransformer);
    }
}
