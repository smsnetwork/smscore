<?php

namespace App\Http\Controllers;

use App\Service\Repository\SemesterService;
use App\Service\Transformer\SemesterTransformer;
use Illuminate\Http\Request;

class SemesterController extends Controller
{
    protected $semesterService;

    public function __construct(SemesterService $semesterService)
    {
        $this->semesterService = $semesterService;
    }

    public function show()
    {
        return $this->response->collection($this->semesterService->all(), new SemesterTransformer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:sessions,name",
        ]);

        $session = $this->semesterService->create($request->all());
        return $this->response->item($session, new SemesterTransformer);
    }

    public function update(Request $request, $id)
    {
        $session = $this->semesterService->get($id);
        $session->update(array_only($request->all(), [
            "name", "is_active"
        ]));
        return $this->response->item($session, new SemesterTransformer);
    }
}
