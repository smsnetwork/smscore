<?php

namespace App\Http\Controllers;

use App\Service\Repository\SessionService;
use App\Service\Transformer\SessionTransformer;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    protected $sessionService;

    public function __construct(SessionService $sessionService)
    {
        $this->sessionService = $sessionService;
    }

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function show()
    {
        return $this->response->collection($this->sessionService->all(), new SessionTransformer);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:sessions,name",
        ]);

        $session = $this->sessionService->create($request->all());
        return $this->response->item($session, new SessionTransformer);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session = $this->sessionService->get($id);
        $session->update(array_only($request->all(), [
            "name", "is_active"
        ]));
        return $this->response->item($session, new SessionTransformer);
    }
}
