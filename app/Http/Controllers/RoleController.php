<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\StoreRolesPost;
use App\Service\Repository\RoleService;
use App\Service\Transformer\RoleTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Fractal\Manager;

class RoleController extends Controller
{
    protected $roleService;
    protected $manager;

    /**
     * RoleController constructor.
     * @param RoleService $roleService
     * @param Manager $manager
     */
    public function __construct(RoleService $roleService, Manager $manager)
    {
        $this->roleService = $roleService;
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->roleService->all(), new RoleTransformer);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->item($this->roleService->get($id), new RoleTransformer);
    }

    /**
     * @param StoreRolesPost $request
     * @return \Dingo\Api\Http\Response
     */
    public function create(StoreRolesPost $request)
    {
        $role = $this->roleService->create($request->all());
        if(!$role) {
            throw new StoreResourceFailedException("Could not create", [
                "message" => "Role could not be created"
            ]);
        }
        return $this->response->item($role, new RoleTransformer);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function attachPermission(Request $request, $id)
    {
        $role = $this->roleService->get($id);
        $permissionId = $this->roleService->validateRolePermissionIds($id, $request->get('permission_id'));
        $role->attachPermission($permissionId);
        if(!$role) {
            throw new StoreResourceFailedException("Could not attach", [
                "message" => "Permission could not be attach"
            ]);
        }
        return $this->success("Permission was attached");
    }

    public function detachPermission($roleId, $permissionId)
    {
        $role = $this->roleService->get($roleId);
        $role->detachPermission($permissionId);
        if(!$role) {
            throw new StoreResourceFailedException("Could not detach", [
                "message" => "Permission could not be remove"
            ]);
        }
        return $this->success("Permission was removed");

    }
    public function delete()
    {

    }
}
