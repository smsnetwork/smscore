<?php

namespace App\Http\Controllers;

use App\Service\Repository\FacultyService;
use App\Service\Transformer\FacultyTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;

class FacultyController extends Controller
{
    protected $facultyService;
    protected $manager;

    public function __construct(FacultyService $facultyService, Manager $manager)
    {
        $this->facultyService = $facultyService;
        $this->manager = $manager;
    }

    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->facultyService->all(), new FacultyTransformer);
    }

    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->item($this->facultyService->get($id), new FacultyTransformer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:faculties,name",
            "code" => "required|unique:faculties,code",
            "programme_id" => "required|exists:programmes,id",
        ]);

        $faculty = $this->facultyService->create($request->all());

        return $this->response->item($faculty, new FacultyTransformer);
    }

    public function update(Request $request, $id)
    {
        $faculty = $this->facultyService->get($id);
        $faculty->update(array_only($request->all(), [
            "name", "code", "programme_id"
        ]));
        return $this->response->item($faculty, new FacultyTransformer);
    }
}
