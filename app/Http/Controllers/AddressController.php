<?php

namespace App\Http\Controllers;

use App\Http\Requests\Address\StoreAddressRequest;
use App\Http\Requests\Address\UpdateAddressRequest;
use App\Service\Repository\AddressService;
use App\Service\Transformer\AddressTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Fractal\Manager;

class AddressController extends Controller
{
    protected $addressService;
    protected $manager;

    public function __construct(AddressService $addressService, Manager $manager)
    {
        $this->addressService = $addressService;
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        $addresses = $this->addressService->all();
        return $this->response->collection($addresses, new AddressTransformer);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        $address = $this->addressService->get($id);
        return $this->response->item($address, new AddressTransformer);
    }

    /**
     * @param StoreAddressRequest $request
     * @return \Dingo\Api\Http\Response
     */
    public function create(StoreAddressRequest $request)
    {
        $address = $this->addressService->create($request->all());
        if (!$address) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "Unable to create address"
            ]);
        }
        return $this->response->item($address, new AddressTransformer);
    }

    public function update(UpdateAddressRequest $request, $id)
    {
        $address = $this->addressService->update($request->all(), $id);
        if (!$address) {
            throw new StoreResourceFailedException('Can not update.', [
                "message" => "Unable to update address"
            ]);
        }
        return $this->response->item($address, new AddressTransformer);
    }
}
