<?php

namespace App\Http\Controllers;

use App\Http\Requests\Subject\StoreSubjectRequest;
use App\Service\Repository\SubjectService;
use App\Service\Transformer\SubjectTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Fractal\Manager;

class SubjectController extends Controller
{
    protected $subjectService;
    protected $manager;

    public function __construct(SubjectService $subjectService, Manager $manager)
    {
        $this->subjectService = $subjectService;
        $this->manager = $manager;
    }

    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        $subjects = $this->subjectService->all();
        return $this->response->collection($subjects, new SubjectTransformer);
    }

    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        $subject = $this->subjectService->get($id);
        return $this->response->item($subject, new SubjectTransformer);
    }

    public function store(StoreSubjectRequest $request)
    {
        $subject = $this->subjectService->create($request->all());
        if(!$subject) {
            throw new StoreResourceFailedException("Could not create", [
                "message" => "Subject could not be created"
            ]);
        }
        return $this->response->item($subject, new SubjectTransformer);
    }

    public function update($id)
    {

    }
}
