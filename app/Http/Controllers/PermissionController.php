<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\StorePermissionsPost;
use App\Service\Repository\PermissionService;
use App\Service\Transformer\PermissionTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

use App\Http\Requests;
use League\Fractal\Manager;

class PermissionController extends Controller
{
    protected $permissionService;
    protected $manager;

    public function __construct(PermissionService $permissionService, Manager $manager)
    {
        $this->permissionService = $permissionService;
        $this->manager = $manager;
    }

    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->permissionService->all(), new PermissionTransformer);
    }

    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->item($this->permissionService->get($id), new PermissionTransformer);
    }

    public function create(StorePermissionsPost $request)
    {
        $permission = $this->permissionService->create($request->all());
        if(!$permission) {
            throw new StoreResourceFailedException("Could not create", [
                "message" => "Permission could not be created"
            ]);
        }
        return $this->response->item($permission, new PermissionTransformer);
    }

    public function attachPermission()
    {

    }

    public function detachPermission()
    {

    }
    public function delete()
    {

    }
}
