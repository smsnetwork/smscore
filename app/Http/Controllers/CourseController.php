<?php

namespace App\Http\Controllers;

use App\Service\Repository\CourseService;
use App\Service\Transformer\CourseTransformer;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    protected $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function show()
    {
        return $this->response->collection($this->courseService->all(), new CourseTransformer);
    }

    public function get($id)
    {
        return $this->response->item($this->courseService->get($id), new CourseTransformer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:courses,name",
            "code" => "required|unique:courses,code",
        ]);

        $course = $this->courseService->create($request->all());
        return $this->response->item($course, new CourseTransformer);
    }

    public function update(Request $request, $id)
    {
        $course = $this->courseService->get($id);
        $course->update(array_only($request->all(), [
            "name", "code"
        ]));
        return $this->response->item($course, new CourseTransformer);
    }
}
