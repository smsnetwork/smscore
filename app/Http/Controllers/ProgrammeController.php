<?php

namespace App\Http\Controllers;

use App\Service\Repository\ProgrammeService;
use App\Service\Transformer\ProgrammeTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;

class ProgrammeController extends Controller
{
    protected $programmeService;
    protected $manager;

    public function __construct(ProgrammeService $programmeService, Manager $manager)
    {
        $this->programmeService = $programmeService;
        $this->manager = $manager;
    }

    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }

        return $this->response->collection($this->programmeService->all(), new ProgrammeTransformer);
    }

    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }

        return $this->response->item($this->programmeService->get($id), new ProgrammeTransformer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:programmes,name",
            "code" => "required|unique:programmes,code",
        ]);

        $programme = $this->programmeService->create($request->all());

        return $this->response->item($programme, new ProgrammeTransformer);
    }

    public function update(Request $request, $id)
    {
        $programmes = $this->programmeService->get($id);
        $programmes->update(array_only($request->all(), [
            "name", "code"
        ]));
        return $this->response->item($programmes, new ProgrammeTransformer);
    }
}
