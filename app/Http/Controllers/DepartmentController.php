<?php

namespace App\Http\Controllers;

use App\Service\Repository\DepartmentService;
use App\Service\Transformer\DepartmentTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;

class DepartmentController extends Controller
{
    protected $departmentService;
    protected $manager;

    public function __construct(DepartmentService $departmentService, Manager $manager)
    {
        $this->departmentService = $departmentService;
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function show(Request $request)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->collection($this->departmentService->all(), new DepartmentTransformer);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function get(Request $request, $id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        return $this->response->item($this->departmentService->get($id), new DepartmentTransformer);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:departments,name",
            "code" => "required|unique:departments,code",
            "faculty_id" => "required|exists:faculties,id"
        ]);

        $department = $this->departmentService->create($request->all());
        return $this->response->item($department, new DepartmentTransformer);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = $this->departmentService->get($id);
        $department->update(array_only($request->all(), [
            "name", "code", "faculty_id"
        ]));
        return $this->response->item($department, new DepartmentTransformer);
    }

    /**
     * @param Request $request
     * @param $departmentId
     * @return \Dingo\Api\Http\Response
     */
    public function addCourses(Request $request, $departmentId)
    {
        $availableCourses = $this->departmentService->getExistingCourse($request->all(), $departmentId);
        $this->departmentService->attachCourses($availableCourses, $departmentId);
        return $this->response->created();
    }

    /**
     * @param $departmentId
     * @param $courseId
     * @return \Dingo\Api\Http\Response
     */
    public function removeCourse($departmentId, $courseId)
    {
        $this->departmentService->detachCourse($departmentId, $courseId);
        return $this->success("Successfully Removed");
    }
}