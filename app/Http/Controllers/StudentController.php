<?php

namespace App\Http\Controllers;

use App\Http\Requests\Ssce\StoreJambRequest;
use App\Http\Requests\Ssce\StoreSsceRequest;
use App\Http\Requests\Students\PostStudentRequest;
use App\Http\Requests\Students\StoreStudentAssociatesRequest;
use App\Service\Repository\StudentService;
use App\Service\Transformer\JambTransformer;
use App\Service\Transformer\SsceTransformer;
use App\Service\Transformer\UserAssociateTransformer;
use App\Service\Transformer\UserTransformer;
use App\Service\Validator\StudentValidator;
use Auth;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use League\Fractal\Manager;

class StudentController extends Controller
{
    protected $studentService;
    protected $studentValidator;
    protected $auth;
    protected $manager;

    public function __construct(StudentService $studentService, StudentValidator $studentValidator, Auth $auth, Manager $manager)
    {
        $this->studentService = $studentService;
        $this->studentValidator = $studentValidator;
        $this->auth = $auth;
        $this->manager = $manager;
    }

    public function get(Request $request, $student_id)
    {
        if($request->has("include")){
            $this->manager->parseIncludes($request->get("include"));
        }
        $student = $this->studentService->get($student_id);
        return $this->response->item($student, new UserTransformer);
    }

    public function login(PostStudentRequest $request)
    {
        $student = $this->studentService->login($request->all());
        if(!$student) {
            throw new AuthenticationException(['User is not authenticated']);
        }
        return $this->response->item($student, new UserTransformer);
    }

    /**
     * @param Request $request
     * @param $student_id
     * @return mixed
     */
    public function createCourse(Request $request, $student_id)
    {
        $isCreated = $this->studentService->attachCourses($request->all(), $student_id);
        if(!$isCreated) {
            throw new StoreResourceFailedException('Could not register course.', [
                "message" => "User has no department or Level"
            ]);
        }
        return $this->success("Courses submitted successfully");
    }

    /**
     * @param Request $request
     * @param $student_id
     * @return mixed
     */
    public function createDepartment(Request $request, $student_id)
    {
        $this->studentValidator->department();
        $response = $this->studentService->createStudentDepartment($request->all(), $student_id);
        if (!$response) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "User already has department for this session, "
            ]);
        }

        return $this->success("Department was added successfully");
    }

    /**
     * @param Request $request
     * @param $student_id
     * @return \Dingo\Api\Http\Response
     */
    public function updateDepartment(Request $request, $student_id)
    {
        $this->studentValidator->department();
        $response = $this->studentService->updateStudentDepartment($request->all(), $student_id);
        if (!$response) {
            throw new StoreResourceFailedException('Can not update.', [
                "message" => "No department record found for this student, "
            ]);
        }

        return $this->success("Student department was updated successfully");
    }

    /**
     * @param Request $request
     * @param $student_id
     * @return mixed
     */
    public function attachLevel(Request $request, $student_id)
    {
        $this->studentValidator->level();
        $response = $this->studentService->attachLevel($request->all(), $student_id);
        if (!$response) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "User already has level for this session, "
            ]);
        }
        return $this->success("Level was added successfully");
    }

    /**
     * @param StoreSsceRequest $request
     * @param $student_id
     * @return mixed
     */
    public function createSsce(StoreSsceRequest $request, $student_id)
    {
        $ssceRecord = $this->studentService->createStudentSsce($request->all(), $student_id);
        if (!$ssceRecord) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "User already has record for this session, "
            ]);
        }
        return $this->response->item($ssceRecord, new SsceTransformer);
    }

    /**
     * @param StoreJambRequest $request
     * @param $studentId
     * @return \Dingo\Api\Http\Response
     */
    public function createJamb(StoreJambRequest $request, $studentId)
    {
        $jambResult = $this->studentService->createStudentJambRecord($request->all(), $studentId);
        if (!$jambResult) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "User already has record for this session, "
            ]);
        }
        return $this->response->item($jambResult, new JambTransformer);
    }

    public function createRelations(StoreStudentAssociatesRequest $request, $studentId)
    {
        $studentAssociate = $this->studentService->createAssociate($request->all(), $studentId);
        if (!$studentAssociate) {
            throw new StoreResourceFailedException('Can not create.', [
                "message" => "User already has record for this session, "
            ]);
        }
        return $this->response->item($studentAssociate, new UserAssociateTransformer);
    }
}