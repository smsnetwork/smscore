<?php

namespace App\Http\Requests\Ssce;

use Dingo\Api\Http\FormRequest;

class StoreSsceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_name' => 'required|string',
            'number' => 'required|string',
            'year' => 'required|string',
            'sitting' => 'required|integer',
            'pin' => 'required|string',
            'serial' => 'required|string',
            'type' => 'required|string',
            'results' => 'required|array',
        ];
    }
}
