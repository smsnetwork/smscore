<?php

namespace App\Http\Requests\Students;

use Dingo\Api\Http\FormRequest;

class StoreStudentAssociatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'mobile' => 'required|min:11',
            'email' => 'email',
            'address' => 'required',
            'relationship' => 'required|string',
            'type' => 'required|string',
        ];
    }
}
