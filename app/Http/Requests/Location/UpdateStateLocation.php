<?php

namespace App\Http\Requests\Location;

use Dingo\Api\Http\FormRequest;

class UpdateStateLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "country_id" => "exists:countries,id",
            "code" => "unique:states,code",
            "name" => "unique:states,name",
        ];
    }
}
