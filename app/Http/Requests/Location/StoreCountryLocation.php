<?php

namespace App\Http\Requests\Location;


use Dingo\Api\Http\FormRequest;

class StoreCountryLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "code" => "required|unique:countries,code",
            "name" => "required|unique:countries,name",
        ];
    }
}
