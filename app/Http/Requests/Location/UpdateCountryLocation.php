<?php

namespace App\Http\Requests\Location;

use Dingo\Api\Http\FormRequest;

class UpdateCountryLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "string|exists:countries,name",
            "code" => "string|exists:countries,code"
        ];
    }
}
