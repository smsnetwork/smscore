<?php

namespace App\Http\Requests\Location;

use Dingo\Api\Http\FormRequest;

class StoreCityLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "country_id" => "required|exists:countries,id",
            "state_id" => "required|exists:states,id",
            "code" => "required|unique:cities,code",
            "name" => "required|unique:cities,name",
        ];
    }
}
