<?php

namespace App\Http\Requests\Address;

use Dingo\Api\Http\FormRequest;

class UpdateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => "exists:users,id",
            "street" => "string",
            "landmark" => "string",
            "country_id" => "exists:countries,id",
            "state_id" => "exists:states,id",
            "city_id" => "exists:cities,id",
        ];
    }
}
