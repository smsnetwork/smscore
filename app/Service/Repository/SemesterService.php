<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:22 PM
 */

namespace App\Service\Repository;

use App\Models\Course;
use App\Models\Semester;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class SemesterService
{
    protected $semester;

    public function __construct(Semester $semester)
    {
        $this->semester = $semester;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public function create($credentials)
    {
        $data = [
            "name" => $credentials['name'],
        ];
        return $this->semester->create($data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->semester->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->semester->findOrFail($id);
    }

    /**
     * @return mixed
     */
    public function getCurrentSemester()
    {
        $currentSemester = $this->semester->where("is_active", "1")->first();
        return $currentSemester->name;
    }
}