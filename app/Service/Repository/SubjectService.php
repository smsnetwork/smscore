<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\Subject;

class SubjectService
{
    protected $subject;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
    }

    public function all()
    {
        return $this->subject->all();
    }

    public function get($id)
    {
        return $this->subject->find($id);
    }

    public function query()
    {
        return $this->subject->query();
    }

    public function create($post)
    {
        $credentials = [
            'name' => $post['name'],
            'code' => $post['code'],
            'slug' => str_slug($post['name']),
        ];
        $subject = $this->subject->create($credentials);
        return $subject;
    }

    public function update($post, $id)
    {
        $subject = $this->get($id);
        $subject->update(array_only($post, [
            'name', 'code'
        ]));
        return $subject;
    }
}