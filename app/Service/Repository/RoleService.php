<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use Bican\Roles\Models\Role;

class RoleService
{
    protected $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function all()
    {
        return $this->role->all();
    }

    public function get($id)
    {
        return $this->role->find($id);
    }

    public function query()
    {
        return $this->role->query();
    }

    public function create($post)
    {
        $post = array_merge($post, ['slug' => str_slug($post['name'])]);
        return $this->role->create($post);
    }

    public function validateRolePermissionIds($roleId, $permissionsIds)
    {
        $role = $this->get($roleId);
        $rolePermissions = $role->permissions;
        $existingPermissionId = $this->pluckId("id", $rolePermissions);
        $unAvailablePermissionId = array_filter($permissionsIds, function ($permission) use ($existingPermissionId) {
            return !in_array($permission, $existingPermissionId);
        });
        return $unAvailablePermissionId;
    }

    /**
     * @param $field
     * @param $data
     * @return static
     */
    public function pluckId($field, $data)
    {
        $collection = collect($data);
        return $collection->pluck($field)->toArray();
    }
}