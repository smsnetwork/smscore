<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\Address;
use Ramsey\Uuid\Uuid;

class AddressService
{
    protected $address;

    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    public function all()
    {
        return $this->address->all();
    }

    public function get($id)
    {
        return $this->address->where('uuid', $id)->first();
    }

    public function query()
    {
        return $this->address->query();
    }

    public function create($post)
    {
        $uuid5 = $this->generateUuid($post['street']);
        $credentials = [
            'uuid' => $uuid5->toString(),
            'user_id' => $post['user_id'],
            'street' => $post['street'],
            'landmark' => $post['landmark'],
            'city_id' => $post['city_id'],
            'state_id' => $post['state_id'],
            'country_id' => $post['country_id'],
            'type' => $post['type'],
        ];
        return $this->address->create($credentials);
    }

    public function generateUuid($name)
    {
        return Uuid::uuid5(Uuid::NAMESPACE_DNS, ($name . uniqid() . time()));
    }

    public function update($post, $id)
    {
        $address = $this->get($id);
        $address->update(array_only($post, [
            'street', 'landmark', 'city_id', 'state_id', 'country_id', 'type'
        ]));
        return $address;
    }
}