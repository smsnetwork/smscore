<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:22 PM
 */

namespace App\Service\Repository;

use App\Models\Level;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class LevelService
{
    protected $level;

    public function __construct(Level $level)
    {
        $this->level = $level;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public function create($credentials)
    {
        try {
            $data = [
                "name" => $credentials['name'],
                "code" => $credentials['code'],
                "slug" => str_slug($credentials['name']),
            ];
            return $this->level->create($data);

        } catch (UnsatisfiedDependencyException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->level->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->level->findOrFail($id);
    }
}