<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\City;

class CityService
{
    protected $city;

    public function __construct(City $city)
    {
        $this->city = $city;
    }

    public function all()
    {
        return $this->city->all();
    }

    public function get($id)
    {
        return $this->city->find($id);
    }

    public function query()
    {
        return $this->city->query();
    }

    public function create($post)
    {
        $credentials = [
            "country_id" => $post['country_id'],
            "state_id" => $post['state_id'],
            "code" => $post['code'],
            "name" => $post['name'],
        ];
        return $this->city->create($credentials);
    }

    public function update($post, $id)
    {
        $city = $this->get($id);
        $city->update(array_only($post, [
            "country_id", "state_id", "code", "name"
        ]));
        return $city;
    }
}