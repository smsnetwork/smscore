<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\Ssce;

class SsceService
{
    protected $ssce;

    public function __construct(Ssce $ssce)
    {
        $this->ssce = $ssce;
    }

    public function all()
    {
        return $this->ssce->all();
    }

    public function get($id)
    {
        return $this->ssce->find($id);
    }

    public function query()
    {
        return $this->ssce->query();
    }

    public function create($post)
    {
        $ssce = $this->ssce->create($post);
        if(!empty($ssce) && isset($post['results'])) {
            $this->createUserSsceResults($post['results'], $ssce->id);
        }
        return $ssce;
    }



    /**
     * @param $post
     * @param $studentId
     * @param $ssceId
     * @return bool
     */
    public function createStudentSsceResults($post, $student, $ssceId)
    {
        $results = [];
        foreach($post as $result) {
            $results[] = [
                'ssce_id' => $ssceId,
                'subject_id' => $result['subject_id'],
                'grade' => $result['grade'],
            ];
        }
        $student->ssce()->attach($results);
        return true;
    }

    public function update($post, $id)
    {
        $ssce = $this->get($id);
        $ssce->update(array_only($post, [
            'school_name', 'year', 'sitting', 'pin', 'serial'
        ]));
        return $ssce;
    }

    public function createUserSsceResults($post, $ssceId)
    {
        $ssce = $this->get($ssceId);
        $ssce->user()->syncWithoutDetaching([$post]);
        return true;
    }

    public function deleteUserSsceResults($ssceId, $userId)
    {
        $ssce = $this->get($ssceId);
        $ssce->user()->detach($userId);
        return true;
    }
}