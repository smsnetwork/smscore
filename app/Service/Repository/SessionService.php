<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:22 PM
 */

namespace App\Service\Repository;


use App\Models\Course;
use App\Models\Session;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class SessionService
{
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public function create($credentials)
    {
        $data = [
            "name" => $credentials['name'],
        ];
        return $this->session->create($data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->session->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->session->findOrFail($id);
    }

    /**
     * @return mixed
     */
    public function getCurrentSession()
    {
        $currentSession = $this->session->where("is_active", "1")->first();
        return $currentSession->name;
    }
}