<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\User;
use DB;
use Ramsey\Uuid\Uuid;

class StudentService
{
    protected $user;
    protected $semesterService;
    protected $sessionService;
    protected $departmentService;
    protected $levelService;
    protected $ssceService;
    protected $jambService;
    protected $userAssociateService;

    public function __construct(
        User $user,
        SessionService $sessionService,
        SemesterService $semesterService,
        DepartmentService $departmentService,
        LevelService $levelService,
        SsceService $ssceService,
        JambService $jambService,
        UserAssociateService $userAssociateService
    )
    {
        $this->user = $user;
        $this->sessionService = $sessionService;
        $this->semesterService = $semesterService;
        $this->departmentService = $departmentService;
        $this->levelService = $levelService;
        $this->ssceService = $ssceService;
        $this->jambService = $jambService;
        $this->userAssociateService = $userAssociateService;
    }

    public function all()
    {
        return $this->user->all();
    }

    public function get($id)
    {
        return $this->user->where("uuid", $id)->first();
    }

    public function query()
    {
        return $this->user->query();
    }

    public function login($credentials)
    {
        $student = $this->user->where('student_id', $credentials['student_id'])
            ->orWhere('matric_number', $credentials['student_id'])->first();
        if (count($student)) {
            if (password_verify($credentials['password'], $student->password)) {
                return $student;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $post
     * @return static
     */
    public function create($post)
    {
        $uuid5 = $this->generateUuid($post['last_name']);
        $credentials = [
            "uuid" => $uuid5->toString(),
            "student_id" => $post['student_id'],
            "matric_number" => $post['matric_number'],
            "mobile" => $post['mobile'],
            "first_name" => $post['first_name'],
            "last_name" => $post['last_name'],
            "middle_name" => $post['middle_name'],
            "email" => $post['email'],
            "password" => bcrypt($post['password']),
            "birthday" => $post['birthday'],
            "gender" => $post['gender'],
        ];
        return $this->user->create($credentials);
    }

    /**
     * @param $name
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function generateUuid($name)
    {
        return Uuid::uuid5(Uuid::NAMESPACE_DNS, ($name . uniqid() . time()));
    }

    public function createStudentCourse($post)
    {
        $this->user->department();
    }

    /**
     * @param $post
     * @param $student_id
     * @return bool
     */
    public function createStudentDepartment($post, $student_id)
    {
        $studentDepartment = $this->checkStudentDepartment($post['department_id'], $student_id);

        if (count($studentDepartment)) {
            return false;
        }
        $student = $this->get($student_id);
        $session = $this->sessionService->getCurrentSession();
        $data = ["session" => $session];

        $student->departments()->syncWithoutDetaching([$post['department_id'] => $data]);

        return true;
    }

    public function updateStudentDepartment($post, $student_id)
    {
        $student = $this->get($student_id);
        $session = $this->sessionService->getCurrentSession();
        $studentDepartment = $student->departments()->wherePivot("session", $session)->first();
        if (!count($studentDepartment)) {
            return false;
        }
        $student->departments()->wherePivot("session", $session)
            ->updateExistingPivot($studentDepartment->pivot->department_id, [
                "department_id" => $post['department_id']
            ]);

        return true;
    }

    public function checkStudentDepartment($department_id, $student_id)
    {
        $student = $this->get($student_id);
        $session = $this->sessionService->getCurrentSession();
        return $student->departments()->wherePivot("department_id", $department_id)
            ->wherePivot("session", $session)->first();
    }

    public function attachCourses($post, $student_id)
    {
        $currentStudentRegistration = $this->studentCurrentCourseRegistration($student_id);
        if (count($currentStudentRegistration)) {
            return false;
        }
        try {
            $transaction = DB::transaction(function () use ($post, $student_id) {
                $studentCurrentDepartment = $this->studentCurrentDepartment($student_id);
                $studentCurrentLevel = $this->studentCurrentLevel($student_id);
                $courses = [];
                foreach ($post as $course) {
                    $courses[] = [
                        "course_id" => $course["course_id"],
                        "department_id" => $studentCurrentDepartment->id,
                        "level_id" => $studentCurrentLevel->id,
                        "course_unit" => $course['course_unit'],
                        "session" => $studentCurrentDepartment->pivot->session,
                        "semester" => $course['semester'],
                    ];
                }
                $this->get($student_id)->courses()->attach($courses);
                return true;
            });
            if ($transaction) {
                return $transaction;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function studentCourseRegistration($student_id)
    {
        $student = $this->studentCurrentDepartment($student_id);
        return $student->courses;
    }

    public function studentCurrentCourseRegistration($student_id)
    {
        $student = $this->get($student_id);
        $studentCourses = $student->courses()->wherePivot("session", $this->sessionService->getCurrentSession())
            ->first();
        return $studentCourses;

    }

    public function studentCurrentDepartment($student_id)
    {
        $student = $this->get($student_id);
        return $student->departments()->wherePivot("session", $this->sessionService->getCurrentSession())->first();
    }

    public function studentDepartments($student_id)
    {
        $student = $this->get($student_id);
        return $student->departments;
    }

    public function attachLevel($post, $student_id)
    {
        $studentCurrentLevel = $this->studentCurrentLevel($student_id);
        if (count($studentCurrentLevel)) {
            return false;
        }
        $student = $this->get($student_id);
        $session = $this->sessionService->getCurrentSession();
        $data = ["session" => $session];

        $student->levels()->syncWithoutDetaching([$post['level_id'] => $data]);

        return true;
    }

    public function studentCurrentLevel($student_id)
    {
        $student = $this->get($student_id);
        return $student->levels()->wherePivot("session", $this->sessionService->getCurrentSession())->first();
    }

    public function studentLevels($student_id)
    {
        $student = $this->get($student_id);
        return $student->levels;
    }

    /**
     * @param $post
     * @param $student_id
     * @return mixed
     */
    public function createStudentSsce($post, $student_id)
    {
        $student = $this->get($student_id);
        $credentials = [
            'user_id' => $student->id,
            'school_name' => $post['school_name'],
            'year' => $post['year'],
            'sitting' => $post['sitting'],
            'pin' => $post['pin'],
            'serial' => $post['serial'],
            'type' => $post['type'],
        ];
        $ssce = $this->ssceService->create($credentials);
        if (!empty($ssce) && isset($post['results'])) {
            $this->ssceService->createStudentSsceResults($post['results'], $student, $ssce->id);
        }
        return $ssce;
    }

    /**
     * @param $post
     * @return mixed
     */
    public function createStudentJambRecord($post, $studentId)
    {
        $student = $this->get($studentId);
        $jamb = $this->jambService->create($post, $student);
        if (!empty($jamb) && isset($post['results'])) {
            $this->jambService->createStudentJambResults($post['results'], $student, $jamb->id);
        }
        return $jamb;
    }

    public function createAssociate($post, $studentId)
    {

        $student = $this->get($studentId);
        $studentAssociate = $this->userAssociateService->create($post, $student);
        return $studentAssociate;
    }
}