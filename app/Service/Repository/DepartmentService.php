<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:22 PM
 */

namespace App\Service\Repository;


use App\Models\Department;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class DepartmentService
{
    protected $department;

    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public function create($credentials)
    {
        try {
            $uuid5 = Uuid::uuid5(Uuid::NAMESPACE_DNS, ($credentials['name'].uniqid().time()));
            $uuid = $uuid5->toString();

            $data = [
                "uuid" => $uuid,
                "name" => $credentials['name'],
                "faculty_id" => $credentials['faculty_id'],
                "code" => $credentials['code'],
                "slug" => str_slug($credentials['name']),
            ];
            return $this->department->create($data);

        } catch (UnsatisfiedDependencyException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->department->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->department->where("uuid", $id)->first();
    }

    /**
     * @param $credentials
     * @param $departmentId
     * @return bool
     */
    public function attachCourses($credentials, $departmentId)
    {
        $department = $this->department->where("uuid", $departmentId)->first();
        $department->courses()->attach($credentials);
        return true;
    }

    /**
     * @param $departmentId
     * @return mixed
     */
    public function existingCourses($departmentId)
    {
        $department = $this->department->where("uuid", $departmentId)->with(['courses'])->first();
        return $department->courses;
    }

    public function detachCourse($departmentId, $courseId)
    {
        $department = $this->department->where("uuid", $departmentId)->first();
        $department->courses()->detach($courseId);
        return true;
    }

    /**
     * @param $field
     * @param $data
     * @return static
     */
    public function pluckId($field, $data)
    {
        $collection = collect($data);
        return $collection->pluck($field)->toArray();
    }

    /**
     * @param $credentials
     * @param $departmentId
     * @return array
     */
    public function getExistingCourse($credentials, $departmentId)
    {
        $existingCourseObjects = $this->existingCourses($departmentId);
        $existingCourses = [];
        foreach($existingCourseObjects as $existingCourseObject){
            $existingCourses[] = $existingCourseObject->pivot->toArray();
        }
        $existingCourseId = $this->pluckId("course_id", $existingCourses);
        $availableCourses = array_filter($credentials, function ($course) use ($existingCourseId) {
            return !in_array($course['course_id'], $existingCourseId);
        });
        return $availableCourses;
    }
}