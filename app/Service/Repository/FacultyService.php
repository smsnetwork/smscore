<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:22 PM
 */

namespace App\Service\Repository;


use App\Models\Faculty;
use App\Models\Programme;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class FacultyService
{
    protected $faculty;

    public function __construct(Faculty $faculty)
    {
        $this->faculty = $faculty;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public function create($credentials)
    {
        try {
            $uuid5 = Uuid::uuid5(Uuid::NAMESPACE_DNS, ($credentials['name'].uniqid().time()));
            $uuid = $uuid5->toString();

            $data = [
                "uuid" => $uuid,
                "name" => $credentials['name'],
                "programme_id" => $credentials['programme_id'],
                "code" => $credentials['code'],
                "slug" => str_slug($credentials['name']),
            ];
            return $this->faculty->create($data);

        } catch (UnsatisfiedDependencyException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->faculty->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->faculty->where("uuid", $id)->first();
    }
}