<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\State;

class StateService
{
    protected $state;

    public function __construct(State $state)
    {
        $this->state = $state;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->state->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->state->find($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->state->query();
    }

    /**
     * @param $post
     * @return mixed
     */
    public function create($post)
    {
        $credentials = [
            "country_id" => $post['country_id'],
            "code" => $post['code'],
            "name" => $post['name'],
        ];
        return $this->state->create($credentials);
    }

    /**
     * @param $post
     * @param $id
     * @return mixed
     */
    public function update($post, $id)
    {
        $state = $this->get($id);
        $state->update(array_only($post, [
            "country_id", "code", "name"
        ]));
        return $state;
    }
}