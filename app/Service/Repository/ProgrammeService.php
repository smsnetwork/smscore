<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:22 PM
 */

namespace App\Service\Repository;


use App\Models\Programme;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class ProgrammeService
{
    protected $programme;

    public function __construct(Programme $programme)
    {
        $this->programme = $programme;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public function create($credentials)
    {
        try {
            $uuid5 = Uuid::uuid5(Uuid::NAMESPACE_DNS, ($credentials['name'].uniqid().time()));
            $uuid = $uuid5->toString();

            $data = [
                "uuid" => $uuid,
                "name" => $credentials['name'],
                "code" => $credentials['code'],
                "slug" => str_slug($credentials['name']),
            ];
            return $this->programme->create($data);

        } catch (UnsatisfiedDependencyException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->programme->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->programme->where("uuid", $id)->first();
    }

    public function update($id)
    {
        $programmes = $this->programme->where("uuid", $id)->first();
        $programmes->update();
    }
}