<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\UserAssociate;

class UserAssociateService
{
    protected $userAssociate;

    public function __construct(UserAssociate $userAssociate)
    {
        $this->userAssociate = $userAssociate;
    }

    public function all()
    {
        return $this->userAssociate->all();
    }

    public function get($id)
    {
        return $this->userAssociate->find($id);
    }

    public function query()
    {
        return $this->userAssociate->query();
    }

    public function create($post, $student)
    {
        $credentials = [
            'user_id' => $student->id,
            'name' => $post['name'],
            'mobile' => $post['mobile'],
            'email' => $post['email'],
            'relationship' => $post['relationship'],
            'address' => $post['address'],
            'type' => $post['type'],
        ];
        $associate = $this->userAssociate->create($credentials);
        return $associate;
    }

    public function update($post, $id)
    {
        $address = $this->get($id);
        $address->update(array_only($post, [
            'street', 'landmark', 'city_id', 'state_id', 'country_id', 'type'
        ]));
        return $address;
    }
}