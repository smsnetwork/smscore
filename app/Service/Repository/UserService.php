<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\User;
use Ramsey\Uuid\Uuid;

class UserService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function all()
    {
        return $this->user->all();
    }

    public function get($id)
    {
        return $this->user->where('uuid', $id)->first();
    }

    public function query()
    {
        return $this->user->query();
    }

    public function create($post)
    {
        $uuid5 = $this->generateUuid($post['last_name']);
        $credentials = [
            'uuid' => $uuid5->toString(),
            'student_id' => $post['student_id'],
            'matric_number' => '',
            'mobile' => $post['mobile'],
            'first_name' => $post['first_name'],
            'last_name' => $post['last_name'],
            'middle_name' => $post['middle_name'],
            'email' => $post['email'],
            'password' => bcrypt($post['password']),
            'birthday' => $post['birthday'],
            'gender' => $post['gender'],
            'type' => $post['type'],
        ];
        return $this->user->create($credentials);
    }

    public function generateUuid($name)
    {
        return Uuid::uuid5(Uuid::NAMESPACE_DNS, ($name . uniqid() . time()));
    }

}