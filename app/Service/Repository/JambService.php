<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\Jamb;
use App\Models\Ssce;

class JambService
{
    protected $jamb;

    public function __construct(Jamb $jamb)
    {
        $this->jamb = $jamb;
    }

    public function all()
    {
        return $this->jamb->all();
    }

    public function get($id)
    {
        return $this->jamb->find($id);
    }

    public function query()
    {
        return $this->jamb->query();
    }

    public function create($post, $student)
    {
        $credentials = [
            'user_id' => $student->id,
            'registration_number' => $post['registration_number'],
            'year' => $post['year'],
            'first_choice' => $post['first_choice'],
            'second_choice' => $post['second_choice'],
            'pin' => $post['pin'],
            'serial' => $post['serial'],
        ];
        $jamb = $this->jamb->create($credentials);
        if(!empty($jamb) && isset($post['results'])) {
            $this->createStudentJambResults($post['results'], $student, $jamb->id);
        }
        return $jamb;
    }

    public function update($post, $id)
    {
        $ssce = $this->get($id);
        $ssce->update(array_only($post, [
            'school_name', 'year', 'sitting', 'pin', 'serial'
        ]));
        return $ssce;
    }



    /**
     * @param $post
     * @param $studentId
     * @param $jambId
     * @return bool
     */
    public function createStudentJambResults($post, $student, $jambId)
    {
        $results = [];
        foreach($post as $result) {
            $results[] = [
                'jamb_id' => $jambId,
                'subject_id' => $result['subject_id'],
                'score' => $result['score'],
            ];
        }
        $student->jambs()->attach($results);
        return true;
    }

    public function deleteUserSsceResults($ssceId, $userId)
    {
        $ssce = $this->get($ssceId);
        $ssce->user()->detach($userId);
        return true;
    }
}