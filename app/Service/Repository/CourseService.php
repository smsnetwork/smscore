<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:22 PM
 */

namespace App\Service\Repository;


use App\Models\Course;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class CourseService
{
    protected $course;

    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    public function create($credentials)
    {
        try {
            $uuid5 = Uuid::uuid5(Uuid::NAMESPACE_DNS, ($credentials['name'].uniqid().time()));
            $uuid = $uuid5->toString();

            $data = [
                "uuid" => $uuid,
                "name" => $credentials['name'],
                "code" => $credentials['code'],
                "slug" => str_slug($credentials['name']),
            ];
            return $this->course->create($data);

        } catch (UnsatisfiedDependencyException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->course->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->course->where("uuid", $id)->first();
    }
}