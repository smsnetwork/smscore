<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use Bican\Roles\Models\Permission;

class PermissionService
{
    protected $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function all()
    {
        return $this->permission->all();
    }

    public function get($id)
    {
        return $this->permission->find($id);
    }

    public function query()
    {
        return $this->permission->query();
    }

    public function create($post)
    {
        $post = array_merge($post, ['slug' => str_slug($post['name'])]);
        return $this->permission->create($post);
    }
}