<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 8:27 PM
 */

namespace App\Service\Repository;

use App\Models\Country;
use Ramsey\Uuid\Uuid;

class CountryService
{
    protected $country;

    public function __construct(Country $country)
    {
        $this->country = $country;
    }

    public function all()
    {
        return $this->country->all();
    }

    public function get($id)
    {
        return $this->country->find($id);
    }

    public function query()
    {
        return $this->country->query();
    }

    public function create($post)
    {
        $credentials = [
            "code" => $post['code'],
            "name" => $post['name'],
        ];
        return $this->country->create($credentials);
    }

    public function update($post, $id)
    {
        $country = $this->get($id);
        $country->update(array_only($post, [
            "name", "code"
        ]));
        return $country;
    }
}