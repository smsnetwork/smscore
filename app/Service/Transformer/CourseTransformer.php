<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:29 PM
 */

namespace App\Service\Transformer;

use App\Models\Course;
use League\Fractal\TransformerAbstract;

class CourseTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'departments'
    ];

    public function transform(Course $course)
    {
        return [
            'uuid' => $course->uuid,
            'id' => $course->id,
            'name' => $course->name,
            'code' => $course->code,
            'slug' => $course->slug,
            'pivot' => $course->pivot,
            'added' => date('Y-m-d', strtotime($course->created_at))
        ];
    }

    /**
     * @param Course $course
     * @return \League\Fractal\Resource\Item
     */
    public function includeDepartments(Course $course)
    {
        $departments = $course->departments;

        return $this->collection($departments, new DepartmentTransformer);
    }
}