<?php

namespace App\Service\Transformer;

use Bican\Roles\Models\Permission;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class PermissionTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'users', 'role'
    ];

    public function transform(Permission $permission)
    {
        return [
            'id' => $permission->id,
            'name' => $permission->name,
            'slug' => $permission->slug,
            'description' => $permission->description
        ];
    }

    /**
     * @param Permission $permission
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUsers(Permission $permission)
    {
        $user = $permission->users;
        return $this->collection($user, new UserTransformer);
    }

    /**
     * @param Permission $permission
     * @return \League\Fractal\Resource\Item
     */
    public function includeRole(Permission $permission)
    {
        $role = $permission->roles;
        return $this->collection($role, new RoleTransformer);
    }
}