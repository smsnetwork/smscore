<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:29 PM
 */

namespace App\Service\Transformer;

use App\Models\Programme;
use League\Fractal\TransformerAbstract;

class ProgrammeTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'faculties', 'departments'
    ];

    public function transform(Programme $programme)
    {
        return [
            'uuid' => $programme->uuid,
            'name' => $programme->name,
            'code' => $programme->code,
            'slug' => $programme->slug,
            'added' => date('Y-m-d', strtotime($programme->created_at))
        ];
    }

    /**
     * @param Programme $programme
     * @return \League\Fractal\Resource\Item
     */
    public function includeFaculties(Programme $programme)
    {
        $faculty = $programme->faculties;

        return $this->collection($faculty, new FacultyTransformer);
    }

    /**
     * @param Programme $programme
     * @return \League\Fractal\Resource\Item
     */
    public function includeDepartments(Programme $programme)
    {
        $author = $programme->departments;

        return $this->collection($author, new DepartmentTransformer);
    }
}