<?php

namespace App\Service\Transformer;

use App\Models\City;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class CityTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'state'
    ];

    public function transform(City $city)
    {
        return [
            'id' => $city->id,
            'name' => $city->name,
            'code' => $city->code,
            'state' => $city->state,
        ];
    }

    /**
     * @param City $city
     * @return \League\Fractal\Resource\Item
     */
    public function includeState(City $city)
    {
        $state = $city->state;
        return $this->item($state, new StateTransformer);
    }
}