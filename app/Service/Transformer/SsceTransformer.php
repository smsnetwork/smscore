<?php

namespace App\Service\Transformer;

use App\Models\Ssce;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class SsceTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user'
    ];

    public function transform(Ssce $ssce)
    {
        return [
            'id' => $ssce->id,
            'school' => $ssce->school_name,
            'sitting' => $ssce->sitting,
            'resultPin' => $ssce->pin,
            'resultSerial' => $ssce->serial,
            'year' => $ssce->year,
            'ssceResult' => $ssce->subjects,
        ];
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Ssce $ssce)
    {
        $user = $ssce->user;

        return $this->item($user, new UserTransformer);
    }
}