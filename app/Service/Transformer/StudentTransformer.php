<?php

namespace App\Service\Transformer;

use App\Models\UserAssociate;
use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class StudentTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'department', 'associates', 'ssce'
    ];

    public function transform(User $user)
    {
        return [
            'uuid' => $user->uuid,
            'name' => sprintf('%s %s %s', $user->last_name, $user->middle_name, $user->first_name),
            'email' => $user->email,
            'mobile' => $user->mobile,
            'gender' => $user->gender,
            'added' => date('Y-m-d', strtotime($user->created_at))
        ];
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     */
    public function includeAssociates(User $user)
    {
        $associates = $user->userAssociate;

        return $this->collection($associates, new UserAssociateTransformer);
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     */
    public function includeSsce(User $user)
    {
        $userSsce = $user->ssce;

        return $this->collection($userSsce, new SsceTransformer);
    }
}