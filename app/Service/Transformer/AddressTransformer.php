<?php

namespace App\Service\Transformer;

use App\Models\Address;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class AddressTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user'
    ];

    public function transform(Address $address)
    {
        return [
            'uuid' => $address->uuid,
            'street' => $address->street,
            'landmark' => $address->landmark,
            'city' => $address->city,
            'state' => $address->state,
            'country' => $address->country,
            'formatted' => sprintf('%s %s %s %s %s', $address->street, $address->landmark,
                $address->city->name, $address->state->name, $address->country->name),
            'added' => date('Y-m-d', strtotime($address->created_at))
        ];
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Address $address)
    {
        $user = $address->user;

        return $this->item($user, new UserTransformer);
    }
}