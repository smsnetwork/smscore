<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:29 PM
 */

namespace App\Service\Transformer;


use App\Models\Course;
use App\Models\Semester;
use League\Fractal\TransformerAbstract;

class SemesterTransformer extends TransformerAbstract
{
    public function transform(Semester $semester)
    {
        return [
            'id' => $semester->id,
            'name' => $semester->name,
            'is_active' => $semester->is_active,
            'added' => date('Y-m-d', strtotime($semester->created_at))
        ];
    }
}