<?php

namespace App\Service\Transformer;

use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'departments', 'jamb', 'ssce', 'levels', 'associates'
    ];

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'uuid' => $user->uuid,
            'lastname' => $user->last_name,
            'firstname' => $user->first_name,
            'middlename' => $user->middle_name,
            'name' => sprintf('%s %s %s', $user->last_name, $user->middle_name, $user->first_name),
            'email' => $user->email,
            'mobile' => $user->mobile,
            'gender' => $user->gender,
            'type' => $user->type,
            'current_level' => !empty($user->levels->first()) ? $user->levels->first() : 'null',
            'current_department' => !empty($user->departments->first()) ? $user->departments->first() : 'null',
            'added' => date('Y-m-d', strtotime($user->created_at))
        ];
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeDepartments(User $user)
    {
        $departments = $user->departments;

        return $this->item($departments, new DepartmentTransformer);
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeJamb(User $user)
    {
        $jamb = $user->jambs()->first();

        return $this->item($jamb, new JambTransformer);
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeSsce(User $user)
    {
        $ssce = $user->ssce;

        return $this->collection($ssce, new SsceTransformer);
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeAddresses(User $user)
    {
        $addresses = $user->addresses;

        return $this->collection($addresses, new AddressTransformer);
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeLevel(User $user)
    {
        $levels = $user->levels;

        return $this->collection($levels, new LevelTransformer);
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     */
    public function includeAssociates(User $user)
    {
        $associates = $user->userAssociate;

        return $this->collection($associates, new UserAssociateTransformer);
    }
}