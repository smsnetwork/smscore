<?php

namespace App\Service\Transformer;

use App\Models\Subject;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class SubjectTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'users',
    ];

    public function transform(Subject $subject)
    {
        return [
            'id' => $subject->id,
            'name' => $subject->name,
            'code' => $subject->code,
            'slug' => $subject->slud,
        ];
    }

    /**
     * @param Subject $subject
     * @return \League\Fractal\Resource\Item
     */
    public function includeUsers(Subject $subject)
    {
        $users = $subject->users;
        return $this->collection($users, new UserTransformer);
    }
}