<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:29 PM
 */

namespace App\Service\Transformer;


use App\Models\Department;
use League\Fractal\TransformerAbstract;

class DepartmentTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'courses', 'faculty'
    ];

    public function transform(Department $department)
    {
        return [
            'uuid' => $department->uuid,
            'id' => $department->id,
            'name' => $department->name,
            'code' => $department->code,
            'slug' => $department->slug,
            //'courses' => $department->pivot,
            'added' => date('Y-m-d', strtotime($department->created_at))
        ];
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     */
    public function includeCourses(Department $department)
    {
        $courses = $department->courses;

        return $this->collection($courses, new CourseTransformer);
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     */
    public function includeFaculty(Department $department)
    {
        $faculty = $department->faculty;

        return $this->item($faculty, new FacultyTransformer);
    }
}