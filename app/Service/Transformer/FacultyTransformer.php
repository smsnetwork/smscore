<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:29 PM
 */

namespace App\Service\Transformer;


use App\Models\Faculty;
use League\Fractal\TransformerAbstract;

class FacultyTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'departments', 'programme'
    ];

    public function transform(Faculty $faculty)
    {
        return [
            'uuid' => $faculty->uuid,
            'id' => $faculty->id,
            'name' => $faculty->name,
            'code' => $faculty->code,
            'slug' => $faculty->slug,
            'added' => date('Y-m-d', strtotime($faculty->created_at))
        ];
    }

    /**
     * @param Faculty $faculty
     * @return \League\Fractal\Resource\Item
     */
    public function includeDepartments(Faculty $faculty)
    {
        $department = $faculty->departments;

        return $this->collection($department, new DepartmentTransformer);
    }

    /**
     * @param Faculty $faculty
     * @return \League\Fractal\Resource\Item
     */
    public function includeProgramme(Faculty $faculty)
    {
        $programme = $faculty->programme;

        return $this->item($programme, new ProgrammeTransformer);
    }
}