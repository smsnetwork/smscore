<?php

namespace App\Service\Transformer;

use App\Models\UserAssociate;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class UserAssociateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user'
    ];

    public function transform(UserAssociate $userAssociate)
    {
        return [
            'id' => $userAssociate->id,
            'name' => $userAssociate->name,
            'mobile' => $userAssociate->mobile,
            'email' => $userAssociate->email,
            'relationship' => $userAssociate->relationship,
            'type' => $userAssociate->type,
            'address' => $userAssociate->address,
            'added_on' => date("Y-m-d", strtotime($userAssociate->created_at)),
        ];
    }

    /**
     * @param UserAssociate $userAssociate
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(UserAssociate $userAssociate)
    {
        $user = $userAssociate->user;

        return $this->item($user, new UserTransformer);
    }
}