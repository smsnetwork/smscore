<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:29 PM
 */

namespace App\Service\Transformer;

use App\Models\Level;
use League\Fractal\TransformerAbstract;

class LevelTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'students'
    ];

    public function transform(Level $level)
    {
        return [
            'id' => $level->id,
            'name' => $level->name,
            'code' => $level->code,
            'added' => date('Y-m-d', strtotime($level->created_at))
        ];
    }

    /**
     * @param Level $level
     * @return \League\Fractal\Resource\Item
     */
    public function includeStudents(Level $level)
    {
        $studentLevel = $level->students;
        return $this->collection($studentLevel, new UserTransformer);
    }
}