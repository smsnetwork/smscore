<?php

namespace App\Service\Transformer;

use App\Models\State;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class StateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        ''
    ];

    public function transform(State $state)
    {
        return [
            'id' => $state->id,
            'name' => $state->name,
            'code' => $state->code,
            'country' => $state->country,
            'cities' => $state->cities
        ];
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     *
    public function includeAuthor(Department $department)
    {
        $author = $book->author;

        return $this->item($author, new AuthorTransformer);
    }*/
}