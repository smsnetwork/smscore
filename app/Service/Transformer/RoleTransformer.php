<?php

namespace App\Service\Transformer;

use Bican\Roles\Models\Role;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class RoleTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'users', 'permissions'
    ];

    public function transform(Role $role)
    {
        return [
            'id' => $role->id,
            'name' => $role->name,
            'slug' => $role->slug,
            'description' => $role->description,
            'level' => $role->level,
        ];
    }

    /**
     * @param Role $role
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUser(Role $role)
    {
        $users = $role->users;
        return $this->collection($users, new UserTransformer);
    }

    public function includePermissions(Role $role)
    {
        $permission = $role->permissions;
        return $this->collection($permission, new PermissionTransformer);
    }
}