<?php

namespace App\Service\Transformer;

use App\Models\Address;
use App\Models\Jamb;
use App\Models\Ssce;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class JambTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user'
    ];

    public function transform(Jamb $jamb)
    {
        return [
            'id' => $jamb->id,
            'registration_number' => $jamb->registration_number,
            'first_choice' => $jamb->first_choice,
            'second_choice' => $jamb->second_choice,
            'year' => $jamb->year,
            'pin' => $jamb->pin,
            'serial' => $jamb->serial,
            'scores' => $jamb->subjects
        ];
    }

    /**
     * @param Jamb $jamb
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Jamb $jamb)
    {
        $user = $jamb->user;

        return $this->item($user, new UserTransformer);
    }
}