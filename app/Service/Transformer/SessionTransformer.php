<?php
/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 11:29 PM
 */

namespace App\Service\Transformer;


use App\Models\Course;
use App\Models\Session;
use League\Fractal\TransformerAbstract;

class SessionTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'semester'
    ];

    public function transform(Session $session)
    {
        return [
            'id' => $session->id,
            'name' => $session->name,
            'is_active' => $session->is_active,
            'added' => date('Y-m-d', strtotime($session->created_at))
        ];
    }

    /**
     * @param Department $department
     * @return \League\Fractal\Resource\Item
     *
    public function includeAuthor(Department $department)
    {
    $author = $book->author;

    return $this->item($author, new AuthorTransformer);
    }*/
}