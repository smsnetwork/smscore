<?php

namespace App\Service\Transformer;

use App\Models\Country;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class CountryTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'cities', 'states'
    ];

    public function transform(Country $country)
    {
        return [
            'id' => $country->id,
            'name' => $country->name,
            'code' => $country->code,
            'states' => $country->states,
            'cities' => $country->cities,
        ];
    }

    /**
     * @param Country $country
     * @return \League\Fractal\Resource\Item
     */
    public function includeStates(Country $country)
    {
        $states = $country->states;

        return $this->item($states, new StateTransformer);
    }

    /**
     * @param Country $country
     * @return \League\Fractal\Resource\Item
     */
    public function includeCities(Country $country)
    {
        $cities = $country->cities;

        return $this->item($cities, new CityTransformer);
    }
}