<?php

namespace App\Service\Validator;
use Dingo\Api\Exception\StoreResourceFailedException;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/17/16
 * Time: 5:54 PM
 */
class LevelValidator
{

    public function __construct()
    {

    }

    public function create()
    {
        $rules = [
            "name" => ["required", "unique:levels,name"],
            "code" => ["required", "unique:levels,code"],
        ];

        $payload = app('request')->only('name', 'code');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new level.', $validator->errors());
        }
    }

    public function update()
    {
        $rules = [
            "name" => ["required", "unique:levels,name"],
            "code" => ["required", "unique:levels,code"],
        ];

        $payload = app('request')->only('name', 'code');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new level.', $validator->errors());
        }
    }
}