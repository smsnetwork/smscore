<?php

namespace App\Service\Validator;
use Dingo\Api\Exception\StoreResourceFailedException;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/17/16
 * Time: 5:54 PM
 */
class StudentValidator
{

    public function __construct()
    {

    }

    public function department()
    {
        $rules = [
            "department_id" => ["required", "exists:departments,id"],
        ];

        $payload = app('request')->only('department_id');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new user.', $validator->errors());
        }
    }

    public function level()
    {
        $rules = [
            "level_id" => ["required", "exists:levels,id"],
        ];

        $payload = app('request')->only('level_id');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new level.', $validator->errors());
        }
    }
}