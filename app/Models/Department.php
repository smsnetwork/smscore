<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = "departments";
    protected $fillable = ["uuid", "name", "code", "faculty_id", "slug"];
    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
    ];

    public function courses()
    {
        return $this->belongsToMany(Course::class, "departments_courses", "department_id", "course_id")
            ->withPivot(["course_id", "unit", "department_id", "optional", "level", "semester"]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function faculty()
    {
        return $this->belongsTo(Faculty::class, "faculty_id", "id");
    }

    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany(Department::class, "users_departments", "department_id", "user_id")
            ->withPivot(["user_id", "department_id", "session"]);
    }
}