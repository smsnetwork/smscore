<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $table = "faculties";
    protected $fillable = ["uuid", "name", "programme_id", "slug", "code"];
    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
    ];

    public function programme()
    {
        return $this->belongsTo(Programme::class, "programme_id", "id");
    }

    public function departments()
    {
        return $this->hasMany(Department::class, "faculty_id", "id");
    }
}
