<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = "levels";
    protected $fillable = ["name", "slug", "code"];
    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
    ];

    public function students()
    {
        return $this->belongsToMany(User::class, "users_levels", "level_id", "user_id")
            ->withPivot(["user_id", "level_id"]);
    }
}
