<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
    protected $table = "programmes";
    protected $fillable = ["uuid", "name", "code", "slug"];
    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
    ];

    public function faculties()
    {
        return $this->hasMany(Faculty::class, "programme_id", "id");
    }

    public function departments()
    {
        return $this->hasManyThrough(Department::class, Faculty::class, "programme_id", "faculty_id", "id");
    }
}