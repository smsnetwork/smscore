<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserAssociate extends Model
{
    protected $table = 'users_associates';
    protected $fillable = ['user_id', 'name', 'mobile', 'email', 'address', 'relationship', 'type'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
