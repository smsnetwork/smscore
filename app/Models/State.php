<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = "states";
    protected $fillable = ["code", "country_id", "name"];

    public function country()
    {
        return $this->belongsTo(Country::class, "country_id", "id");
    }

    public function cities()
    {
        return $this->hasMany(City::class, "state_id", "id");
    }
}
