<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ssce extends Model
{
    protected $table = 'ssce';
    protected $fillable = ['user_id', 'school_name', 'number', 'year', 'sitting', 'pin', 'serial', 'type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'ssce_results', 'ssce_id', 'user_id')
            ->withPivot(['ssce_id', 'subject_id', 'user_id', 'grade']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'ssce_results', 'ssce_id', 'subject_id')
            ->withPivot(['ssce_id', 'subject_id', 'user_id', 'grade']);
    }
}
