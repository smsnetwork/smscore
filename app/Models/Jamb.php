<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Jamb extends Model
{
    protected $table = 'jambs';
    protected $fillable = ['user_id', 'registration_number', 'year', 'first_choice', 'second_choice', 'pin', 'serial'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'jambs_results', 'jamb_id', 'user_id')
            ->withPivot(['jamb_id', 'user_id', '']);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'jambs_results', 'jamb_id', 'subject_id')
            ->withPivot(['jamb_id', 'user_id', 'score', 'subject_id']);
    }
}
