<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "courses";
    protected $fillable = ["uuid", "name", "slug", "code"];
    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
    ];

    /**
     * @return mixed
     */
    public function departments()
    {
        return $this->belongsToMany(Department::class, "departments_courses", "course_id", "department_id")
            ->withPivot(["course_id", "unit", "department_id", "optional", "level", "semester"]);
    }

    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany(User::class, "users_courses", "course_id", "user_id")
            ->withPivot(["course_id", "unit", "department_id", "optional", "level", "semester"]);
    }
}