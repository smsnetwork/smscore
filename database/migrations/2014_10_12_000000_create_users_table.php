<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('student_id');
            $table->string('matric_number');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('email')->unique();
            $table->date('birthday');
            $table->enum('gender', ['male', 'female']);
            $table->string('mobile');
            $table->string('password');
            $table->enum('is_admitted', ["0", "1"]);
            $table->string('type');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('users_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('level_id');
            $table->string('session');
            $table->timestamps();
        });

        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->string('user_id');
            $table->string('street');
            $table->string('landmark');
            $table->string('city_id');
            $table->string('state_id');
            $table->string('country_id');
            $table->string('type');
            $table->timestamps();
        });
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('country_id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('country_id');
            $table->string('state_id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('code');
            $table->timestamps();
        });
        Schema::create('users_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('department_id');
            $table->unsignedInteger('level_id');
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('course_unit');
            $table->string('semester');
            $table->string('session');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('users_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('department_id');
            $table->string('session');
            $table->timestamps();
        });

        Schema::create('jambs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_number');
            $table->unsignedBigInteger('user_id');
            $table->text('first_choice');
            $table->text('second_choice');
            $table->string('pin');
            $table->string('serial');
            $table->string('year');
            $table->timestamps();
        });

        Schema::create('jambs_results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('jamb_id');
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('score');
            $table->timestamps();
        });

        Schema::create('ssce', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->string('school_name');
            $table->string('number');
            $table->string('year');
            $table->unsignedInteger('sitting');
            $table->string('pin');
            $table->string('serial');
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('ssce_results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('ssce_id');
            $table->unsignedBigInteger('subject_id');
            $table->string('grade');
            $table->timestamps();
        });

        Schema::create("subjects", function (Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("code");
            $table->string("slug");
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("ssce_grades", function (Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("slug");
            $table->string("code");
            $table->timestamps();
        });
        Schema::create("users_associates", function (Blueprint $table){
            $table->increments("id");
            $table->string("user_id");
            $table->string("name");
            $table->string("email");
            $table->string("mobile");
            $table->string("address");
            $table->string("relationship");
            $table->string("type");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('users_levels');
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('states');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('levels');
        Schema::dropIfExists('users_courses');
        Schema::dropIfExists('users_departments');
        Schema::dropIfExists('ssce');
        Schema::dropIfExists('ssce_results');
        Schema::dropIfExists('jambs');
        Schema::dropIfExists('jambs_results');
        Schema::dropIfExists("subjects");
        Schema::dropIfExists("ssce_grades");
        Schema::dropIfExists("users_associates");
    }
}
