<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('courses_id');
            $table->unsignedBigInteger('ca');
            $table->unsignedBigInteger('ca_total');
            $table->unsignedBigInteger('exam');
            $table->unsignedBigInteger('exam_total');
            $table->unsignedBigInteger('overall_total');
            $table->unsignedBigInteger('grade');
            $table->string('semester');
            $table->string('session');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('results_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from');
            $table->integer('to');
            $table->string('grade');
            $table->string('definition');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
        Schema::dropIfExists('results_grades');
    }
}
