<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("sessions", function (Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->enum("is_active", ["0", "1"])->default(0);
            $table->timestamps();
        });

        Schema::create("semesters", function (Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->enum("is_active", ["0", "1"])->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("programmes", function (Blueprint $table){
            $table->increments("id");
            $table->string("uuid");
            $table->string("slug");
            $table->string("code");
            $table->string("name");
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("faculties", function (Blueprint $table){
            $table->increments("id");
            $table->string("uuid");
            $table->unsignedInteger("programme_id");
            $table->string("slug");
            $table->string("code");
            $table->string("name");
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("departments", function (Blueprint $table){
            $table->increments("id");
            $table->string("uuid");
            $table->unsignedInteger("faculty_id");
            $table->string("slug");
            $table->string("code");
            $table->string("name");
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("courses", function (Blueprint $table){
            $table->increments("id");
            $table->string("uuid");
            $table->string("name");
            $table->string("code");
            $table->string("slug");
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create("departments_courses", function (Blueprint $table){
            $table->increments("id");
            $table->string("department_id");
            $table->string("course_id");
            $table->string("unit");
            $table->string("level");
            $table->string("semester");
            $table->string("optional");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("sessions");
        Schema::dropIfExists("semesters");
        Schema::dropIfExists("programmes");
        Schema::dropIfExists("faculties");
        Schema::dropIfExists("departments");
        Schema::dropIfExists("courses");
        Schema::dropIfExists("departments_courses");
    }
}
