<?php

use Illuminate\Database\Seeder;

class OauthClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateTime = date("Y-m-d H:i:s", time());
        //seeds the Oauth API client
        DB::table('oauth_clients')->insert([
            'secret' => '3PVgGbvWMmYOK0tDzVpOvTk1FEyEKIN08xYtI5ZN',
            'name' => 'SMS Client',
            'redirect' => 'http://core.sms.dev',
            'password_client' => '1',
            'personal_access_client' => '0',
            'revoked' => '0',
            'created_at' => $dateTime,
            'updated_at' => $dateTime,
        ]);
    }
}
