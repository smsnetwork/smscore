<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        User::create([
            'uuid' => $faker->uuid,
            'student_id' => "MX5678",
            'matric_number' => "2015/04/00390",
            'first_name' => "Tosin",
            'middle_name' => "Seun",
            'last_name' => "Akomolafe",
            'mobile' => "07035038924",
            'email' => "tos.akomolafe@gmail.com",
            'password' => bcrypt('123456'),
            'birthday' => '1990-01-09',
            'type' => 'diploma',
            'gender' => 'male',
            'remember_token' => str_random(10)
        ]);
    }
}
